<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\BukuTamuController;
use App\Http\Controllers\Backend\BukuController as BackendBukuController;


use App\Http\Controllers\Backend\BukuController as BackendBukuTamuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/backend/buku/manage', [BackendBukuTamuController::class, 'index'])->name('backend.manage.buku');

Route::get('/backend/buku/edit/{buku}', [BackendBukuTamuController::class, 'edit'])->name('backend.edit.buku');
Route::post('/backend/buku/edit/process/{buku}', [BackendBukuTamuController::class, 'edit_process'])->name('backend.edit.process.buku');
Route::delete('/backend/delete/{id}', [BackendBukuController::class, 'destroy'])->name('backend.delete.mentor');

Route::get('/backend/buku', [BackendBukuController::class, 'index'])->name('backend.manage.buku');
Route::get('/backend/create/buku', [BackendBukuController::class, 'create'])->name('backend.create.buku');
Route::post('/backend/create/process/buku', [BackendBukuController::class, 'create_process'])->name('backend.create.process.buku');


Route::get('/bukutamu', [BukuTamuController::class, 'index'])->name("frontend.bukutamu.index");

Route::post('/bukutamu/process', [BukuTamuController::class, 'process'])->name("frontend.bukutamu.process");
