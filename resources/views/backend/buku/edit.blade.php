@extends('layouts.app')

@section('title')
    Buku Tamu | Edit #ID {{ $buku->id }}
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

    <script>
        $(function () {
            $('textarea[name=tujuan]').summernote({height: 200});
            $('textarea[name=kesepakatan]').summernote({height: 200});
        });
    </script>
@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Buku Tamu | Edit #ID {{ $buku->id }} </div>
                <div class="card-body">
                    <div class="card-body">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {!! Session::get('success') !!}
                            </div>
                        @endif
                        <form action="{{ route('backend.edit.process.buku', $buku->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 mb-3">
                                        <div class="mb-3">
                                            <div class="mb-2 @error('nama') is-invalid fw-bold @enderror">Nama</div>
                                            <input type="text" name="nama" placeholder="Masukkan Nama..." class="form-control @error('nama') is-invalid text-danger @enderror" value="{{ $buku->nama }}">
                                            @error('nama')
                                                <small class="text-danger">{!! $message !!}</small>
                                            @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="mb-2 @error('instansi') is-invalid fw-bold @enderror">Instansi</div>
                                            <input type="text" name="instansi" placeholder="" class="form-control @error('instansi') is-invalid text-danger @enderror" value="{{ $buku->instansi }}">
                                            @error('instansi')
                                                <small class="text-danger">{!! $message !!}</small>
                                            @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="mb-2 @error('alamat') is-invalid fw-bold @enderror">Alamat</div>
                                            <input type="text" name="alamat" placeholder="Masukkan Alamat..." class="form-control @error('alamat') is-invalid text-danger @enderror" value="{{ $buku->alamat }}">
                                            @error('alamat')
                                                <small class="text-danger">{!! $message !!}</small>
                                            @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="mb-2 @error('telpon') is-invalid fw-bold @enderror">No. Telepon</div>
                                            <input type="text" name="telpon" placeholder="Masukkan Nomor Anda Yang Aktif..." class="form-control @error('telpon') is-invalid text-danger @enderror" value="{{ $buku->telpon }}">
                                            @error('telpon')
                                                <small class="text-danger">{!! $message !!}</small>
                                            @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="mb-2 @error('tujuan') is-invalid fw-bold @enderror">Maksud/Tujuan</div>
                                            <textarea class="form-control @error('tujuan') is-invalid @enderror" name="tujuan" type="text">{!! $buku->tujuan !!}</textarea>
                                            @error('tujuan')
                                                <div class="text-danger small">{!! $message !!}</div>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <div class="mb-2 @error('kesepakatan') is-invalid fw-bold @enderror">Hasil/Kesepakatan</div>
                                            <textarea class="form-control @error('kesepakatan') is-invalid @enderror" name="kesepakatan" type="text">{!! $buku->kesepakatan !!}</textarea>
                                            @error('kesepakatan')
                                                <div class="text-danger small">{!! $message !!}</div>
                                            @enderror
                                        </div>
                                            <button class="btn btn-primary">Edit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
