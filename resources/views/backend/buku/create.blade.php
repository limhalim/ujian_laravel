@extends('layouts.app')

@section('title')
    Buku Tamu | Create
@endsection

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(function () {
            $('textarea[name=desc]').summernote({height: 200});
            $('input[name=image]').change(function(){
                imagePreview(this);
            });
        });
        function imagePreview(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $("#preview").removeClass("d-none");
                    $("#preview").attr("src",e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Buku Tamu | Create') }}</div>
                <div class="card-body">
                    <form id="contactForm" action="{{ route('backend.create.process.buku') }}" method="post" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col-xs-12 col-sm-12  col-md-12 mb-3">
                                   
                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('nama') text-danger fw-bold @enderror">
                                        Nama:
                                        </div>
                                        <input type="text" name="nama" value="{{ old('nama') }}" placeholder="Nama"
                                        class="form-control">
                                        @error('nama')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('instansi') text-danger fw-bold @enderror">
                                        Instansi:
                                        </div>
                                        <input type="text" name="instansi" value="{{ old('instansi') }}" placeholder="Instansi"
                                        class="form-control">
                                        @error('instansi')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('alamat') text-danger fw-bold @enderror">
                                        Alamat:
                                        </div>
                                        <input type="text" name="alamat" value="{{ old('alamat') }}" placeholder="alamat"
                                        class="form-control">
                                        @error('alamat')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('telpon') text-danger fw-bold @enderror">
                                        Telepon:
                                        </div>
                                        <input type="text" name="telpon" value="{{ old('telpon') }}" placeholder="Telpon"
                                        class="form-control">
                                        @error('telpon')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('tujuan') text-danger fw-bold @enderror">
                                        Tujuan:
                                        </div>
                                        <input type="text" name="tujuan" value="{{ old('tujuan') }}" placeholder="tujuan"
                                        class="form-control">
                                        @error('tujuan')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="mb-2 @error('kesepakatan') text-danger fw-bold @enderror">
                                        Kesepakatan:
                                        </div>
                                        <input type="text" name="kesepakatan" value="{{ old('kesepakatan') }}" placeholder="kesepakatan"
                                        class="form-control">
                                        @error('kesepakatan')
                                        <div class="text-danger small" >{!! $message !!}</div>
                                        @enderror
                                    </div>

            

                                

                                    <button class="btn btn-primary btn-xl" id="submitButton" type="submit">Create Mentor</button>
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
