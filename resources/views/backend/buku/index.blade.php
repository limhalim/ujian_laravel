
@extends('layouts.app')

@section('title')
    Categories | Backend
@endsection

@section('javascript')
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function () {
        $('table').DataTable({
            "pageLength" : 50
        });
    });
</script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-full">
            <div class="card">
                <div class="card-header">
                    {{ __('Buku Tamu') }}

                </div>

                <div class="card-body">
                    @if (Session::has('error'))
                        <div class="alert alert-success">
                            {!! Session::get('error') !!}
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>nama</th>
                                <th>instansi</th>
                                <th>alamat</th>
                                <th>telpon</th>
                                <th>tujuan</th>
                                <th>kesepakatan</th>
                                <th>hari</th>
                                <th>Action</th>

                            </thead>
                            @foreach ($buku as $item => $value)
                            <tbody>
                                <th>{!! $value->id !!}</th>
                                <td>{!! $value->nama !!}</td>
                                <td>{!! $value->instansi !!}</td>
                                <td>{!! $value->alamat !!}</td>
                                <td>{!! $value->telpon !!}</td>
                                <td>{!! $value->tujuan !!}</td>
                                <td>{!! $value->kesepakatan !!}</td>
                                <td>{!! $value->hari !!}</td>
                                <td></td>

                                <td></td>
                                <td>
                                    {{-- <a href="#" class="btn btn-sm btn-primary"><i class="fas fa-search pe-1"></i> Show</a> --}}
                                    <a href="{{ route('backend.edit.buku', $value->id) }}" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt pe-1"></i>Edit</a>
                                    <form action="{{ route('backend.delete.mentor', $value->id) }}" method="post" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-danger">
                                            <i class="fas fa-trash-alt pe-1"> Destroy</i>
                                        </button>
                                    </form>
                                </td>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
