<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('title')</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @yield('css')
        <style>
            .form-control:disabled, .form-control[readonly] {
                background-color: transparent !important;
                opacity: 1;
            }
            .quantity{
                top: 10px;
                right: -7px
            }
        </style>
        <script>

        </script>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <section class="page-section" id="contact">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Buku Tamu KODEIN</h2>
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-xl-7">
                        <form id="contactForm" action="{{ route('frontend.bukutamu.process') }}" method="post">
                            @csrf
                            @if (Session::has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            <div class="form-floating mb-3">
                                <input disabled="disabled" class="form-control" name="nama" type="text" value="{{ $hari[date('N')] }}"/>
                                <label for="name">Pada Hari</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input disabled="disabled" class="form-control" name="instansi" type="text" value="{{ date('d M Y') }}"/>
                                <label for="email">Tanggal</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input value="{{ old('nama') }}" class="form-control @error('nama') is-invalid @enderror" name="nama" type="text" placeholder="Nama"/>
                                <label for="nama">Nama Tamu</label>
                                @error('nama')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                                @if (Session::has('error'))
                                <div class="text-danger small">
                                    {{ Session::get('error') }}
                                </div>
                                @endif
                            </div>
                            <div class="form-floating mb-3">
                                <input value="{{ old('instansi') }}" class="form-control @error('instansi') is-invalid @enderror" name="instansi" type="text" placeholder="Instansi"/>
                                <label for="instansi">Nama Instansi</label>
                                @error('instansi')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                                
                            </div>
                            <div class="form-floating mb-3">
                                <input value="{{ old('alamat') }}" class="form-control @error('alamat') is-invalid @enderror" name="alamat" type="text" placeholder="Alamat"/>
                                <label for="alamat">Alamat</label>
                                @error('alamat')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input value="{{ old('telpon') }}" class="form-control @error('telpon') is-invalid @enderror" name="telpon" type="number" placeholder="Telpon"/>
                                <label for="telpon">No. Telp./HP</label>
                                @error('telpon')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control @error('tujuan') is-invalid @enderror" name="tujuan" type="text" placeholder="Masukan tujuan..." style="height: 10rem">{{ old('tujuan') }}</textarea>
                                <label for="tujuan">Maksud/Tujuan</label>
                                @error('tujuan')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control @error('kesepakatan') is-invalid @enderror" name="kesepakatan" type="text" placeholder="Kesepakatan..." style="height: 10rem">{{ old('kesepakatan') }}</textarea>
                                <label for="kesepakatan">Hasil/Kesepakatan</label>
                                @error('kesepakatan')
                                    <div class="text-danger small" >{!! $message !!}</div>
                                @enderror
                            </div>
                            <button class="btn btn-success w-100 btn-xl" id="submitButton" type="submit">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            Jl. Jeruk Purut, No.18E
                            <br />
                            Jeruk Purut, Cilandak Barat, Jakarta Selatan
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Kodein</h4>
                        <p class="lead mb-0">
                            Sekolah Developer Indeonesia <br>
                            <a href="http://kodein-ppdb.com">Gabung Sekarang</a>
                            .
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container">
                <small>
                    Copyright &copy; KODEIN{{!! date('Y') > 2022 ? 2022 : 2022 . ' - ' . date('Y') !!}}
                </small>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        @yield('javascript')
    </body>
</html>