<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\BukuTamu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BukuTamuController extends Controller
{
    public function index()
    {
        $hari = [
            1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        ];

        return view('frontend.bukutamu.index', compact('hari'));
    }

    public function process(Request $request)
    {
        $rule = [
            'nama' => 'required',
            'instansi' => 'required',
            'alamat' => 'required',
            'telpon' => 'required',
            'tujuan' => 'required',
            'kesepakatan' => 'required',
            
        ];

        $message =[
            'nama.required' => '<strong>Nama</strong> Harus diisi!',
            'instansi.required' => '<strong>Instansi</strong> Harus diisi!',
            'alamat.required' => '<strong>Alamat</strong> Harus diisi!',
            'telpon.required' => '<strong>Telpon</strong> Harus diisi!',
            'tujuan.required' => '<strong>Tujuan</strong> Harus diisi!',
            'kesepakatan.required' => '<strong>Kesepakatan</strong> Harus diisi!',
        ];

        $validator = Validator::make($request->all(), $rule, $message);
        $hari = [
            1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        ];
        

        if ($validator->fails()) {
            return redirect()->route('frontend.bukutamu.index')->withErrors($validator)->withInput();
        } else {
            if (ctype_alpha($request->nama)) {
                BukuTamu::create([
                    'hari' => $hari[date('N')],
                    'nama' => $request->nama,
                    'instansi' => $request->instansi,
                    'alamat' => $request->alamat,
                    'telpon' => $request->telpon,
                    'tujuan' => $request->tujuan,
                    'kesepakatan' => $request->kesepakatan,
                ]);

            $pesan = "Selamat datang {$request->nama}! terimakasih sudah mengisi buku tamu";
            return redirect()->route('frontend.bukutamu.index')->with('success', $pesan);
            } else {
                return redirect()->route('frontend.bukutamu.index')->with('error', 'Nama harus berisi alphabet!')->withInput();
            }
        };



    }
}
