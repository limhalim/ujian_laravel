<?php

namespace App\Http\Controllers\Backend;

use App\Models\BukuTamu;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BukuController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $hari = [
            1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        ];

        $buku = BukuTamu::get();
        return view('backend.buku.index', compact('hari', 'buku'));

    }

    public function edit($id)
    {
        if ($id == null) {
            return redirect()->route('backend.manage.buku')->with('error', 'The ID is empty!');
        } else {
            $buku = BukuTamu::find($id);

            if ($buku) {
                return view('backend.buku.edit', compact('buku'));
            } else {
                return redirect()->route('backend.manage.buku')->with('error', "The #ID {$id} not found in Database!");
            }
        }
    }

    public function edit_process(Request $request, $id)
    {

        request()->validate([
            'nama'         => 'required',
            'instansi'     => 'required',
            'alamat'       => 'required',
            'telpon'       => 'required',
            'tujuan'       => 'required',
            'kesepakatan'  => 'required'
        ]);


        BukuTamu::where('id', $id)
            ->update(([

                'nama'          => $request->nama,
                'instansi'      => $request->instansi,
                'alamat'        => $request->alamat,
                'telpon'        => $request->telpon,
                'tujuan'        => $request->tujuan,
                'kesepakatan'   => $request->kesepakatan
            ]));

        return redirect()->route('backend.manage.buku')->with('success', 'Item Edited Successfully');

    }

    public function create(){
        return view('backend.buku.create');
    }

    public function create_process(Request $request){
        request()->validate([
            'nama'         => 'required',
            'instansi'     => 'required',
            'alamat'       => 'required',
            'telpon'       => 'required',
            'tujuan'       => 'required',
            'kesepakatan'  => 'required'
        ]);

        $hari = [
            1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        ];

        BukuTamu::create(([
            'hari' => $hari[date('N')],
            'nama' => $request->nama,
            'instansi' => $request->instansi,
            'alamat' => $request->alamat,
            'telpon' => $request->telpon,
            'tujuan' => $request->tujuan,
            'kesepakatan' => $request->kesepakatan,
            ]));

        return redirect()->route('backend.manage.buku')->with('success', 'Item Created Successfully');
    }

    public function destroy($id){
        $buku = BukuTamu::find($id);

        $buku->delete();

        return redirect()->route('backend.manage.buku')->with('success', 'Item has been deleted');
    }


}
